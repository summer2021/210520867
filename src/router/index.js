import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// 引入NProgress进度条
import NProgress from "nprogress";
import "nprogress/nprogress.css";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    meta: { title: "首页" },
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    meta: { title: "登录" },
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
];

const router = new VueRouter({
  routes,
});

// 全局使用NProgress
router.beforeEach((to, from, next) => {
  NProgress.start();
  next();
});

router.afterEach((to) => {
  window.document.title = to.meta.title ? to.meta.title : "LMP";
  NProgress.done();
});

export default router;
